import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

import {Sim} from '../../shared/models/sim.model';
import {SimService} from '../../core/services/sim.service';
import {MatDialog} from '@angular/material';
import {SimAssignComponent} from './simAssign.component';
import {UserService} from '../../core/services/user.service';
import {MessageModalComponent} from '../../shared/components/message-modal/message-modal.component';
import {NotifierService} from 'angular-notifier';

@Component({
  selector: 'bell-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.sass']
})
export class SearchComponent implements OnInit {

  searchForm: FormGroup;
  simInfo: Sim;
  simStatus: string;
  results: boolean;
  loader: boolean;
  isAdmin: boolean;

  constructor(private router: Router, private formBuilder: FormBuilder, private userService: UserService,
              private simService: SimService, public dialog: MatDialog, private notifierService: NotifierService) {
  }

  ngOnInit() {
    this.loader = false;
    this.searchForm = this.formBuilder.group({
      identification: [null, [Validators.required]]
    });
    this.isAdmin = this.userService.userData.role === 'ADMIN';
  }

  search() {
    this.loader = true;
    const identification = this.searchForm.get('identification').value;
    this.simService.getSimInfo(identification).subscribe(response => {
      this.showSim(response);
    }, error => {
      this.showSimError(error);
    });
  }

  openAssignDialog() {
    const dialogRef = this.dialog.open(SimAssignComponent, {
      width: '400px',
      panelClass: 'paper-background',
      data: {simIcc: this.simInfo.icc}
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result === 'ASSIGNED') {
        const messageDialogRef = this.dialog.open(MessageModalComponent, {
          width: '400px',
          panelClass: 'paper-background',
          data: {
            type: 'exitoso',
            message: 'Simcard Asignada Satisfactoriamente'
          }
        });

        messageDialogRef.afterClosed().subscribe(() => {
          this.search();
        });
      } else {
        this.dialog.open(MessageModalComponent, {
          width: '400px',
          panelClass: 'paper-background',
          data: {
            type: 'error',
            message: 'Error al asignar Simcard'
          }
        });
      }
    });
  }

  unassign(e) {
    this.loader = true;
    e.preventDefault();
    this.simService.unassignSim(this.simInfo.number)
      .subscribe(
        (response) => {
          this.showSim(response);
          this.dialog.open(MessageModalComponent, {
            width: '400px',
            panelClass: 'paper-background',
            data: {
              type: 'exitoso',
              message: 'Simcard Desasignada Satisfactoriamente'
            }
          });
        },
        error => this.showSimError(error)
      );
  }

  showSim(response) {
    if (response.subDistributorName.trim() === this.userService.userData.subdistributorName.trim()
      || this.userService.userData.role === 'ADMIN') {
      this.loader = false;
      const today = new Date().toISOString();
      this.simInfo = response;
      this.results = true;

      if (response.activationDate === undefined && response.dueDate > today) {
        this.simStatus = 'Disponible';
      } else if (response.activationDate !== undefined && response.dueDate > today) {
        this.simStatus = 'Activada';
      } else if (response.dueDate < today) {
        this.simStatus = 'Vencida';
      }

      switch (response.type) {
        case 1:
          this.simInfo.typeName = 'Prepago';
          break;
        case 2:
          this.simInfo.typeName = 'Libre';
          break;
      }
    } else {
      this.loader = false;
      this.results = false;
      this.notifierService.notify('error', 'Simcard no encontrada');
    }
  }

  showSimError(error) {
    this.loader = false;
    this.results = false;

    if (error.status === 404) {
      this.notifierService.notify('error', 'Simcard no encontrada');
    } else {
      this.notifierService.notify('error', 'Lo sentimos, ocurrio un error');
    }
  }
}
