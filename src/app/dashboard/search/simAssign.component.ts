import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {EmployeeService} from '../../core/services/employee.service';
import {UserService} from '../../core/services/user.service';
import {SimService} from '../../core/services/sim.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

@Component({
  selector: 'bell-free-assign',
  templateUrl: './simAssign.component.html',
  styleUrls: ['./simAssign.component.sass']
})
export class SimAssignComponent implements OnInit {

  assignForm: FormGroup;
  employeeControl = new FormControl('', [Validators.required]);
  employees: any[];
  employeeIdentification;
  loader: boolean;

  constructor(private formBuilder: FormBuilder, private userService: UserService, private employeeService: EmployeeService,
              private simcardService: SimService, @Inject(MAT_DIALOG_DATA) public data: any,
              private dialogRef: MatDialogRef<SimAssignComponent>) {
  }

  ngOnInit() {
    this.assignForm = this.formBuilder.group({
      identification: [null, [Validators.required]]
    });
    if (this.userService.userData.role === 'ADMIN') {
      this.employeeService.listWholesalers()
        .subscribe(response => {
          this.employees = response;
        });
    } else {
      this.employeeService.listWholesalersByLeader(this.userService.userData.subdistributorName)
        .subscribe(response => {
          this.employees = response;
        });
    }
  }

  assignSimcard() {
    this.simcardService.assignSim(this.data.simIcc, this.employeeIdentification).subscribe(response => {
      this.dialogRef.close('ASSIGNED');
    });
  }

}
