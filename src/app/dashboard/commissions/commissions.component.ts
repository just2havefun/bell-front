import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Chart} from 'chart.js';

import {UserService} from '../../core/services/user.service';
import {ReportsService} from '../../core/services/reports.service';
import {SubDistributorService} from '../../core/services/subdistributor.service';
import {Wholesaler} from '../../shared/models/wholesaler.model';
import {COLORS, getAllColors} from '../../shared/utils/colors';
import {NotifierService} from 'angular-notifier';
import {formatMoney} from '../../shared/utils/formatter';

@Component({
  selector: 'bell-commissions',
  templateUrl: './commissions.component.html',
  styleUrls: ['./commissions.component.sass']
})
export class CommissionsComponent implements OnInit {

  periods: Array<any>;
  commissionsChartByPeriod: any;
  projectionChartByPeriod: any;
  periodSelected: string;
  subSelected: string;
  wholesalerSelected: string;
  identification: number;
  subList: Array<string>;
  employeeList: Array<Wholesaler>;
  userType: string;
  employeeRole: string;
  loader = false;
  commissionResume: any[];
  displayedColumns: string[] = ['description', 'prepaid', 'free'];
  totalCommission;
  allWholesalers = false;

  constructor(private router: Router, private userService: UserService, private reportsService: ReportsService,
              private subDisService: SubDistributorService, private notifierService: NotifierService) {
  }

  ngOnInit() {
    this.employeeRole = this.userService.userData.role;
    this.userType = this.userService.userData.type;
    this.subList = this.userService.subList;
    this.subSelected = this.userService.userData.subdistributorName;
    if (this.userService.userData.type === 'SUBDISTRIBUTOR' || this.userService.userData.role === 'ADMIN') {
      this.refreshSubdistributor();
    } else {
      this.wholesalerSelected = this.userService.userData.employee.identification.toString();
      this.refreshPeriods();
    }
  }

  refreshSubdistributor() {
    this.loader = true;
    this.periods = [];
    this.subDisService.getWholesalersByLeader(this.subSelected)
      .subscribe((response) => {
        this.employeeList = response.wholesalers;
        this.loader = false;
      });
    if (this.allWholesalers) {
      this.refreshPeriods();
    }
  }

  refreshWholesaler() {
    this.refreshPeriods();
  }

  refreshPeriods() {
    this.periods = [];
    this.periodSelected = undefined;
    if (this.allWholesalers) {
      this.loader = true;
      this.reportsService.getAvailablePeriodsByName(this.subSelected).subscribe(response => {
        this.periods = [];
        response.forEach(element => {
          this.periods.push({value: element, viewValue: element});
        });
        this.loader = false;
      }, error => {
        this.notifierService.notify('error', 'Lo sentimos, ocurrio un error');
      });
    } else if (this.wholesalerSelected !== undefined) {
      this.loader = true;
      this.reportsService.getAvailablePeriods(+this.wholesalerSelected).subscribe(response => {
        this.periods = [];
        response.forEach(element => {
          this.periods.push({value: element, viewValue: element});
        });
        this.loader = false;
      }, error => {
        this.notifierService.notify('error', 'Lo sentimos, ocurrio un error');
      });
    }
  }

  refreshCharts(): void {
    if (this.allWholesalers) {
      this.getSubCommissionsByPeriod();
    } else {
      this.getWholesalerCommissionsByPeriod();
      this.getProjectionByPeriod();
    }
  }

  getSubCommissionsByPeriod(): void {
    this.loader = true;
    this.reportsService.getCommissionByPeriodSubs(this.subSelected, this.periodSelected).subscribe(response => {
      this.commissionResume = [];
      const labels = [];
      const commissionsData = [];
      let prepaidTotal = 0;
      let freeTotal = 0;
      response.forEach((commission) => {
        const prepaidSubtotal = commission.prepaidCommission - commission.prepaidRete - commission.prepaidIca;
        const freeSubtotal = commission.freeCommission - commission.freeRete - commission.freeIca;
        prepaidTotal += prepaidSubtotal;
        freeTotal += freeSubtotal;
        labels.push(commission.employeeName);
        commissionsData.push((prepaidSubtotal + freeSubtotal).toFixed(0));

        // COMMISSION RESUME

        this.totalCommission = formatMoney(prepaidSubtotal + freeSubtotal);
        this.commissionResume.push(
          {
            description: commission.employeeName,
            prepaid: formatMoney(prepaidSubtotal),
            free: formatMoney(freeSubtotal)
          },
        );
      });
      this.totalCommission = formatMoney(prepaidTotal + freeTotal);

      // WholesalersChart
      this.destroyCommissionChart();
      this.commissionsChartByPeriod = new Chart('commissionsByPeriod', {
        type: 'doughnut',
        data: {
          labels: labels,
          datasets: [{
            data: commissionsData,
            backgroundColor: getAllColors()
          }]
        },
        options: {
          responsive: true,
          tooltips: {
            callbacks: {
              label: function (tooltipItem, data) {
                const label = data.labels[tooltipItem.index];
                const datasetLabel = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                return label + ': ' + formatMoney(datasetLabel);
              }
            }
          }
        }
      });

      this.loader = false;
    }, error => {
      this.notifierService.notify('error', 'Lo sentimos, ocurrio un error');
    });
  }

  getWholesalerCommissionsByPeriod(): void {
    this.loader = true;
    this.reportsService.getCommissionsByPeriod(+this.wholesalerSelected, this.periodSelected).subscribe(response => {
      const prepaidSubtotal = response.prepaidCommission - response.prepaidRete - response.prepaidIca;
      const freeSubtotal = response.freeCommission - response.freeRete - response.freeIca;

      this.destroyCommissionChart();
      this.commissionsChartByPeriod = new Chart('commissionsByPeriod', {
        type: 'doughnut',
        data: {
          labels: [
            'Libre',
            'Prepago'
          ],
          datasets: [{
            data: [freeSubtotal.toFixed(0), prepaidSubtotal.toFixed(0)],
            backgroundColor: [
              COLORS.blue,
              COLORS.green
            ]
          }]
        },
        options: {
          responsive: true,
          tooltips: {
            callbacks: {
              label: function (tooltipItem, data) {
                const label = data.labels[tooltipItem.index];
                const datasetLabel = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                return label + ': ' + formatMoney(datasetLabel);
              }
            }
          }
        }
      });

      // COMMISSION RESUME

      this.totalCommission = formatMoney(prepaidSubtotal + freeSubtotal);
      this.commissionResume = [
        {description: 'Ganancias', prepaid: formatMoney(response.prepaidCommission), free: formatMoney(response.freeCommission)},
        {description: 'Ica', prepaid: formatMoney(response.prepaidIca), free: formatMoney(response.freeIca)},
        {description: 'Retencion', prepaid: formatMoney(response.prepaidRete), free: formatMoney(response.freeRete)},
        {description: 'Subtotal', prepaid: formatMoney(prepaidSubtotal), free: formatMoney(freeSubtotal)},
      ];
      this.loader = false;
    }, error => {
      this.notifierService.notify('error', 'Lo sentimos, ocurrio un error');
    });
  }

  getProjectionByPeriod(): void {
    this.reportsService.getProjectionByPeriod(+this.wholesalerSelected, this.periodSelected).subscribe(response => {
      this.projectionChartByPeriod = new Chart('projectionByPeriod', {
        type: 'doughnut',
        data: {
          labels: [
            'Libre',
            'Prepago'
          ],
          datasets: [{
            data: [response.freeCommission.toFixed(0), response.prepaidCommission.toFixed(0)],
            backgroundColor: [
              COLORS.blue,
              COLORS.green
            ]
          }]
        },
        options: {
          responsive: true,
          tooltips: {
            callbacks: {
              label: function (tooltipItem, data) {
                const label = data.labels[tooltipItem.index];
                const datasetLabel = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                return label + ': ' + formatMoney(datasetLabel);
              }
            }
          }
        }
      });
    }, error => {
      this.notifierService.notify('error', 'Lo sentimos, ocurrio un error');
    });
  }

  destroyCommissionChart() {
    if (this.commissionsChartByPeriod !== undefined) {
      this.commissionsChartByPeriod.destroy();
    }
  }
}
