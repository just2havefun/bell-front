import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Chart} from 'chart.js';

import {UserService} from '../../core/services/user.service';
import {ReportsService} from '../../core/services/reports.service';
import {SubDistributorService} from '../../core/services/subdistributor.service';
import {NotifierService} from 'angular-notifier';
import {formatMoney} from '../../shared/utils/formatter';

@Component({
  selector: 'bell-recharges',
  templateUrl: './recharges.component.html',
  styleUrls: ['./recharges.component.sass']
})
export class RechargesComponent implements OnInit {

  periods: Array<any>;
  periodSelected: string;
  subSelected: string;
  identification: number;
  subList: Array<string>;
  userType: string;
  employeeRole: string;
  loader = false;
  commissionResume = [];
  displayedColumns: string[] = ['employeeName', 'prepaidRecharge', 'freeRecharge'];
  totalCommission;

  constructor(private router: Router, private userService: UserService, private reportsService: ReportsService,
              private subDisService: SubDistributorService, private notifierService: NotifierService) {
  }

  ngOnInit() {
    this.employeeRole = this.userService.userData.role;
    this.userType = this.userService.userData.type;
    this.subList = this.userService.subList;
    this.subSelected = this.userService.userData.subdistributorName;
    if (this.userService.userData.type === 'SUBDISTRIBUTOR' || this.userService.userData.role === 'ADMIN') {
      this.refreshSubdistributor();
    }
  }

  refreshSubdistributor() {
    this.loader = true;
    this.periods = [];
    this.refreshPeriods();
  }

  refreshPeriods() {
    this.periods = [];
    this.periodSelected = undefined;
    this.loader = true;
    this.reportsService.getAvailablePeriodsByName(this.subSelected).subscribe(response => {
      this.periods = [];
      response.forEach(element => {
        this.periods.push({value: element, viewValue: element});
      });
      this.loader = false;
    }, error => {
      this.notifierService.notify('error', 'Lo sentimos, ocurrio un error');
    });
  }

  refreshCharts(): void {
    this.getSubRechargesByPeriod();
  }

  getSubRechargesByPeriod(): void {
    this.loader = true;
    this.commissionResume = [];
    this.reportsService.getSubRechargeReportByPeriod(this.subSelected, this.periodSelected).subscribe(response => {
      let prepaidTotal = 0;
      let freeTotal = 0;
      response.forEach((commission) => {
        const prepaidSubtotal = commission.prepaidRecharge;
        const freeSubtotal = commission.freeRecharge;
        prepaidTotal += prepaidSubtotal;
        freeTotal += freeSubtotal;

        // COMMISSION RESUME

        this.commissionResume.push(
          {
            employeeName: commission.employeeName,
            prepaidRecharge: formatMoney(prepaidSubtotal),
            freeRecharge: formatMoney(freeSubtotal)
          },
        );
      });
      this.totalCommission = formatMoney(prepaidTotal + freeTotal);
      console.log(this.commissionResume);
      this.loader = false;
    }, error => {
      this.notifierService.notify('error', 'Lo sentimos, ocurrio un error');
    });
  }

}
