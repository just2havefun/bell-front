import {Component, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material';
import {UserService} from '../../core/services/user.service';
import {UpdateNumberComponent} from './components/update-number/update-number.component';

@Component({
  selector: 'bell-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.sass']
})
export class UploadComponent implements OnInit {

  COMPONENTS = [
    {
      name: 'Actualización Numeros',
      component: UpdateNumberComponent,
      visible: this.userService.userData.role === 'ADMIN'
    }
  ];

  constructor(public dialog: MatDialog, private userService: UserService) {
  }

  ngOnInit() {
  }

  openDialog(component) {
    this.dialog.open(component, {
      width: '400px',
      panelClass: 'paper-background'
    });
  }

}
