import {Component, OnInit, ViewChild} from '@angular/core';
import {UploadService} from '../../../../core/services/upload.service';
import {UserService} from '../../../../core/services/user.service';
import {MatDialogRef} from '@angular/material';

@Component({
  selector: 'bell-update-number',
  templateUrl: './update-number.component.html',
  styleUrls: ['./update-number.component.sass']
})
export class UpdateNumberComponent implements OnInit {

  @ViewChild('fileInput') fileInput;

  file: File;
  uploading = false;

  constructor(private uploadService: UploadService, private userService: UserService,
              private dialogRef: MatDialogRef<UpdateNumberComponent>) {
  }

  ngOnInit() {
  }

  addFile() {
    this.fileInput.nativeElement.click();
  }

  uploadFile() {
    this.uploading = true;
    this.uploadService.uploadUpdateNumbersByIccFile(this.fileInput.nativeElement.files, this.userService.userData.email)
      .subscribe(() => {
        this.uploading = false;
        alert('Exitoso');
        this.dialogRef.close();
      });
  }
}
