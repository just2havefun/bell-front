import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {UserService} from '../../core/services/user.service';
import {ReportsService} from '../../core/services/reports.service';
import {getAllColors} from '../../shared/utils/colors';
import {Chart} from 'chart.js';
import {NotifierService} from 'angular-notifier';

@Component({
  selector: 'bell-production',
  templateUrl: './production.component.html',
  styleUrls: ['./production.component.sass']
})
export class ProductionComponent implements OnInit {

  periods: Array<any>;
  productionChartByPeriod: any;
  periodSelected: string;
  subSelected: string;
  wholesalerSelected: string;
  identification: number;
  subList: Array<string>;
  userType: string;
  employeeRole: string;
  loader = false;
  productionResume: any[];
  displayedColumns: string[] = ['description', 'prepaid', 'free'];
  totalProduction;
  allWholesalers = false;

  constructor(private router: Router, private userService: UserService, private reportsService: ReportsService,
              private notifierService: NotifierService) {
  }

  ngOnInit() {
    this.employeeRole = this.userService.userData.role;
    this.userType = this.userService.userData.type;
    this.subList = this.userService.subList;
    this.subSelected = this.userService.userData.subdistributorName;
    if (this.userService.userData.type === 'SUBDISTRIBUTOR' || this.userService.userData.role === 'ADMIN') {
      this.refreshSubdistributor();
    } else {
      this.wholesalerSelected = this.userService.userData.employee.identification.toString();
      this.refreshPeriods();
    }
  }

  refreshSubdistributor() {
    this.loader = true;
    this.periods = [];
    this.refreshPeriods();
  }

  refreshPeriods() {
    this.periods = [];
    this.periodSelected = undefined;
    this.loader = true;
    this.reportsService.getProductionAvailablePeriodsByName(this.subSelected).subscribe(response => {
      this.periods = [];
      response.forEach(element => {
        this.periods.push({value: element, viewValue: element});
      });
      this.loader = false;
    }, error => {
      this.notifierService.notify('error', 'Lo sentimos, ocurrio un error');
    });
  }

  refreshCharts(): void {
    this.getSubProductionByPeriod();
  }

  getSubProductionByPeriod(): void {
    this.loader = true;
    this.reportsService.getProductionByPeriodSubs(this.subSelected, this.periodSelected).subscribe(response => {
      this.productionResume = [];
      const labels = [];
      const productionData = [];
      let prepaidTotal = 0;
      let freeTotal = 0;
      response.forEach((production) => {
        prepaidTotal += production.prepaidProduction;
        freeTotal += production.freeProduction;
        labels.push(production.employeeName);
        productionData.push((production.prepaidProduction + production.freeProduction).toFixed(0));

        // COMMISSION RESUME

        this.productionResume.push(
          {
            description: production.employeeName,
            prepaid: this.formatMoney(production.prepaidProduction),
            free: this.formatMoney(production.freeProduction)
          },
        );
      });
      this.totalProduction = this.formatMoney(prepaidTotal + freeTotal);

      // WholesalersChart
      this.destroyCommissionChart();
      this.productionChartByPeriod = new Chart('productionByPeriod', {
        type: 'doughnut',
        data: {
          labels: labels,
          datasets: [{
            data: productionData,
            backgroundColor: getAllColors()
          }]
        },
        options: {
          responsive: true
        }
      });

      this.loader = false;
    }, error => {
      this.notifierService.notify('error', 'Lo sentimos, ocurrio un error');
    });
  }

  formatMoney(number) {
    const formatter = new Intl.NumberFormat('es-CO', {
      style: 'currency',
      currency: 'COP',
      minimumFractionDigits: 0
    });
    return formatter.format(number);
  }

  destroyCommissionChart() {
    if (this.productionChartByPeriod !== undefined) {
      this.productionChartByPeriod.destroy();
    }
  }
}
