import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';

import {DashboardComponent} from './dashboard/dashboard.component';
import {SearchComponent} from './search/search.component';
import {AdminComponent} from './admin/admin.component';
import {CommissionsComponent} from './commissions/commissions.component';
import {ReportsComponent} from './reports/reports.component';
import {ProductionComponent} from './production/production.component';
import {SimPackComponent} from './sim-pack/sim-pack.component';
import {UploadComponent} from './upload/upload.component';
import {RechargesComponent} from './recharges/recharges.component';

const ROUTES: Routes = [
  {
    path: '',
    component: DashboardComponent,
    children: [
      {
        path: 'search',
        component: SearchComponent
      },
      {
        path: 'pack',
        component: SimPackComponent
      },
      {
        path: 'employees',
        component: AdminComponent
      },
      {
        path: 'commissions',
        component: CommissionsComponent
      },
      {
        path: 'production',
        component: ProductionComponent
      },
      {
        path: 'recharges',
        component: RechargesComponent
      },
      {
        path: 'reports',
        component: ReportsComponent
      },
      {
        path: 'upload',
        component: UploadComponent
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(ROUTES)
  ],
  exports: [
    RouterModule
  ]
})
export class RoutingModule {
}
