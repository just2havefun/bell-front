import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

import {Wholesaler} from '../../shared/models/wholesaler.model';
import {Subdistributor} from '../../shared/models/subdistributor.model';
import {UserService} from '../../core/services/user.service';
import {SubDistributorService} from '../../core/services/subdistributor.service';
import {DistributorService} from '../../core/services/distributor.service';
import {MatDialog} from '@angular/material';
import {CreateWholesalerComponent} from './createWholesaler.component';
import {ApplicationUser} from '../../shared/models/application-user.model';
import {MessageModalComponent} from '../../shared/components/message-modal/message-modal.component';
import {EmployeeService} from '../../core/services/employee.service';
import {NotifierService} from 'angular-notifier';

@Component({
  selector: 'bell-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.sass']
})
export class AdminComponent implements OnInit {

  panelOpenState = false;
  basicInfoForm: FormGroup;
  distributorForm: FormGroup;
  subDistributorForm: FormGroup;
  updateSub: FormGroup;
  subdistributors: Array<Subdistributor>;
  wholesalers: Array<Wholesaler>;
  leaderName: string;
  employeeType: string;
  employeeRole: string;
  subSelected: string;
  wholesalerLeader: string;
  subList: Array<string>;
  adminFilter: boolean;

  constructor(private router: Router, private formBuilder: FormBuilder,
              private userService: UserService, private subDisService: SubDistributorService,
              private employeeService: EmployeeService,
              private disService: DistributorService, public dialog: MatDialog,
              private notifierService: NotifierService) {
  }

  ngOnInit() {
    this.subList = this.userService.subList;
    this.employeeType = this.userService.userData.type;
    this.employeeRole = this.userService.userData.role;
    this.basicInfoForm = this.formBuilder.group({
      mail: [this.userService.userData.email, [Validators.required]],
      newPassword: [null, []]
    });

    this.distributorForm = this.formBuilder.group({
      mail: [null, [Validators.required]],
      password: [null, [Validators.required]]
    });

    this.subDistributorForm = this.formBuilder.group({
      subdis: [null]
    });

    this.updateSub = this.formBuilder.group({
      email: [null],
      commission: [null]
    });

    if (this.employeeType === 'SUBDISTRIBUTOR' && this.employeeRole !== 'ADMIN') {
      this.getSubDisByDefault();
      this.subSelected = this.userService.userData.subdistributorName;
    } else if (this.employeeType === 'DISTRIBUTOR' || this.employeeRole === 'ADMIN') {
      this.subSelected = 'Todos';
      this.getDis();
    }
  }

  changePassword(): void {
    this.userService.changePassword(this.basicInfoForm.get('newPassword').value).subscribe(response => {
      this.dialog.open(MessageModalComponent, {
        width: '400px',
        data: {
          type: 'exitoso',
          message: 'Contraseña actualizada correctamente'
        }
      });
    }, error => {
      this.dialog.open(MessageModalComponent, {
        width: '400px',
        data: {
          type: 'error',
          message: 'Error al actualizar contraseña'
        }
      });
    });
  }

  getDis() {
    const distributor = this.userService.userData.email;
    if (this.employeeRole !== 'ADMIN') {
      this.disService.getSubDis(distributor).subscribe(response => {
        this.subdistributors = response.subDistributors;
      }, error => {
        this.notifierService.notify('error', 'Lo sentimos, ocurrio un error');
      });
    } else {
      this.subDisService.getSubList().subscribe(response => {
        this.subdistributors = response;
      }, error => {
        this.notifierService.notify('error', 'Lo sentimos, ocurrio un error');
      });
    }
  }

  getSubDis() {
    if (this.subSelected !== 'Todos') {
      this.adminFilter = true;
      this.subDisService.getWholesalersByLeader(this.subSelected).subscribe(response => {
        this.wholesalers = response.wholesalers;
      }, error => {
        this.notifierService.notify('error', 'Lo sentimos, ocurrio un error');
      });
    } else {
      this.adminFilter = false;
    }
  }

  getSubDisByDefault() {
    const subDistributor = this.userService.userData.email;
    this.subDisService.getWholesalersByLeaderMail(subDistributor).subscribe(response => {
      this.wholesalers = response.wholesalers;
      this.leaderName = response.name;
    }, error => {
      this.notifierService.notify('error', 'Lo sentimos, ocurrio un error');
    });
  }

  openCreateWholesalerDialog(subdistributorName: String) {
    if (subdistributorName === undefined) {
      subdistributorName = this.subSelected;
    }
    const dialogRef = this.dialog.open(CreateWholesalerComponent, {
      width: '400px',
      panelClass: 'paper-background',
      data: {
        subdistributorName: subdistributorName
      }
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result === 'CREATED') {
        const messageDialogRef = this.dialog.open(MessageModalComponent, {
          width: '400px',
          panelClass: 'paper-background',
          data: {
            type: 'exitoso',
            message: 'Empleado creado satisfactoriamente'
          }
        });

        messageDialogRef.afterClosed().subscribe(() => {
          this.updateList();
        });
      }
    });
  }

  updateSubdistributor(subdistributor) {
    if (subdistributor.commission > 100) {
      subdistributor.commission = 100;
    }
    if (subdistributor.commission < 0) {
      subdistributor.commission = 0;
    }
    this.subDisService.updateSubDistributor(subdistributor).subscribe(
      () => this.getDis()
    );
  }

  updateWholesaler(wholesaler) {
    if (wholesaler.commission > 100) {
      wholesaler.commission = 100;
    }
    if (wholesaler.commission < 0) {
      wholesaler.commission = 0;
    }
    this.employeeService.updateWholesaler(wholesaler).subscribe(
      () => {
        this.updateList();
      }
    );

    if (wholesaler.user != null) {
      wholesaler.email = wholesaler.user.email;
    }

    if (wholesaler.email != null) {
      const applicationUser = new ApplicationUser();
      applicationUser.email = wholesaler.email;
      applicationUser.employee = {
        identification: wholesaler.identification
      };
      applicationUser.type = 'EMPLOYEE';
      applicationUser.role = 'GENERAL';

      this.userService.createUser(applicationUser).subscribe(() => {
        this.updateList();
      }, error => {
      });
    }
  }

  deleteWholesaler(wholesaler) {
    if (confirm('¿Está Seguro de querer eliminarlo?')) {
      this.employeeService.deleteWholesaler(wholesaler).subscribe(
        () => {
          this.updateList();
        }
      );
    }
  }

  private updateList() {
    if (this.employeeType === 'SUBDISTRIBUTOR' && this.employeeRole !== 'ADMIN') {
      this.getSubDisByDefault();
    } else if (this.employeeRole === 'ADMIN') {
      this.getSubDis();
    }
  }
}
