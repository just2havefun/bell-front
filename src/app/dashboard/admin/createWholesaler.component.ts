import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {UserService} from '../../core/services/user.service';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material';
import {SubDistributorService} from '../../core/services/subdistributor.service';
import {Wholesaler} from '../../shared/models/wholesaler.model';
import {EmployeeService} from '../../core/services/employee.service';
import {MessageModalComponent} from '../../shared/components/message-modal/message-modal.component';

@Component({
  selector: 'bell-create-wholesaler',
  templateUrl: './createWholesaler.component.html',
  styleUrls: ['./createWholesaler.component.sass']
})
export class CreateWholesalerComponent implements OnInit {

  createForm: FormGroup;
  employeeControl = new FormControl('', [Validators.required]);
  subs: any[];
  admin: boolean;
  subName: string;
  newWholesaler: Wholesaler = {};
  locatable = false;
  commission = 0;
  loader: boolean;

  constructor(private formBuilder: FormBuilder, private userService: UserService, private subDistributorService: SubDistributorService,
              private employeeService: EmployeeService, @Inject(MAT_DIALOG_DATA) public data: any, public dialog: MatDialog,
              private dialogRef: MatDialogRef<CreateWholesalerComponent>) {
  }

  ngOnInit() {
    this.createForm = this.formBuilder.group({
      identification: [null, [Validators.required]],
      name: [null, [Validators.required]],
      phone: [null, [Validators.required]],
      address: [null, [Validators.required]],
      commission: [null, [Validators.required]]
    });

    if (this.data.subdistributorName != null) {
      this.subName = this.data.subdistributorName;
    }

    if (this.userService.userData.role === 'ADMIN') {
      this.admin = true;
      this.subDistributorService.getSubList()
        .subscribe(response => {
          this.subs = response;
        });
    } else {
      this.admin = false;
    }
  }

  createWholesaler() {
    this.newWholesaler = this.createForm.value;
    this.newWholesaler.role = 'WHOLESALER';
    if (this.admin) {
      this.newWholesaler.leaderName = this.subName;
      this.newWholesaler.locatable = this.locatable;
    } else {
      this.newWholesaler.leaderName = this.userService.userData.subdistributorName;
      this.newWholesaler.locatable = false;
    }

    if (this.commission > 100) {
      this.commission = 100;
    }
    if (this.commission < 0) {
      this.commission = 0;
    }
    this.newWholesaler.commission = String(this.commission);

    this.employeeService.createWholesaler(this.newWholesaler).subscribe(() => {
      this.dialogRef.close('CREATED');
    }, (err) => {
      if (err.status === 409) {
        this.dialog.open(MessageModalComponent, {
          width: '400px',
          data: {
            type: 'error',
            message: 'Lo sentimos pero ya existe un empleado con ese número de identificación'
          }
        });
      }
    });
  }

}
