import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'bell-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.sass']
})
export class DashboardComponent implements OnInit {

  displayMenu: boolean;

  constructor() { }

  ngOnInit() {
    this.displayMenu = false;
  }

  openMenu(): void {
    this.displayMenu = true;
  }

  closeMenu(): void {
    this.displayMenu = false;
  }
}
