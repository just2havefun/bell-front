import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RoutingModule} from './routing.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SharedModule} from '../shared/shared.module';
import {DashboardComponent} from './dashboard/dashboard.component';
import {SearchComponent} from './search/search.component';
import {AdminComponent} from './admin/admin.component';
import {CommissionsComponent} from './commissions/commissions.component';
import {FreeReportComponent} from './reports/components/freeReport/freeReport.component';
import {SimAssignComponent} from './search/simAssign.component';
import {CreateWholesalerComponent} from './admin/createWholesaler.component';
import {ReportsComponent} from './reports/reports.component';
import {WholesalerReportComponent} from './reports/components/wholesaler-report/wholesaler-report.component';
import {PrepaidReportComponent} from './reports/components/prepaid-report/prepaid-report.component';
import {ProductionComponent} from './production/production.component';
import {SimPackComponent} from './sim-pack/sim-pack.component';
import {NotifierModule} from 'angular-notifier';
import {WholesalerCommissionComponent} from 'app/dashboard/reports/components/wholesaler-commission/wholesaler-commission.component';
import {UpdateNumberComponent} from './upload/components/update-number/update-number.component';
import {UploadComponent} from './upload/upload.component';
import {RechargesComponent} from './recharges/recharges.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    RoutingModule,
    NotifierModule
  ],
  declarations: [
    DashboardComponent,
    SearchComponent,
    AdminComponent,
    CommissionsComponent,
    FreeReportComponent,
    PrepaidReportComponent,
    SimAssignComponent,
    ReportsComponent,
    CreateWholesalerComponent,
    WholesalerReportComponent,
    WholesalerCommissionComponent,
    ProductionComponent,
    SimPackComponent,
    UploadComponent,
    UpdateNumberComponent,
    RechargesComponent
  ],
  entryComponents: [
    FreeReportComponent,
    PrepaidReportComponent,
    SimAssignComponent,
    CreateWholesalerComponent,
    WholesalerReportComponent,
    WholesalerCommissionComponent,
    UpdateNumberComponent
  ]
})
export class DashboardModule {
}
