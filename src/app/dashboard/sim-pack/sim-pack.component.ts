import {Component, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UserService} from '../../core/services/user.service';
import {SimService} from '../../core/services/sim.service';
import {Sim} from '../../shared/models/sim.model';
import {SubDistributorService} from '../../core/services/subdistributor.service';
import {MatStepper} from '@angular/material';
import {NotifierService} from 'angular-notifier';

@Component({
  selector: 'bell-sim-pack',
  templateUrl: './sim-pack.component.html',
  styleUrls: ['./sim-pack.component.sass']
})
export class SimPackComponent implements OnInit {

  simNumber = '';
  packSize: number;
  results: boolean;
  loader: boolean;
  notFound: boolean;
  isAdmin: boolean;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  simcards: Sim[] = [];
  resumeSimcards: Sim[] = [];
  states = [];
  resumeStates = [];
  packedSimcardNumber;
  subSelected;
  wholesalerSelected;
  subList = [];
  employeeList = [];
  updatingPack = false;
  packedSub = '';
  packedEmployee = '';
  toDeleteFromPack = [];
  newPack = true;

  @ViewChild('simNumberInput') private simNumberInput;
  @ViewChild('stepper') private myStepper: MatStepper;

  constructor(private router: Router, private formBuilder: FormBuilder, private userService: UserService,
              private simService: SimService, private _formBuilder: FormBuilder, private subDisService: SubDistributorService,
              private notifierService: NotifierService) {
  }

  ngOnInit() {
    this.loader = false;
    this.notFound = false;
    this.isAdmin = this.userService.userData.role === 'ADMIN';
    this.subList = this.userService.subList;
    this.subSelected = this.userService.userData.subdistributorName;
    this.refreshSubdistributor();
    this.firstFormGroup = this._formBuilder.group({
      packSize: ['', Validators.required],
      newPack: []
    });
  }

  refreshSubdistributor() {
    this.loader = true;
    this.subDisService.getWholesalersByLeader(this.subSelected)
      .subscribe((response) => {
        this.employeeList = response.wholesalers;
        this.loader = false;
      });
  }

  search() {
    if (this.simNumber !== '') {
      this.notFound = false;
      this.loader = true;
      this.simService.getSimInfo(this.simNumber).subscribe(response => {
        if (response.subDistributorName.trim() === this.userService.userData.subdistributorName.trim() || this.isAdmin) {
          if (this.simcards.length === 0 && response.pack !== 0 && response.pack !== undefined && !this.newPack) {
            this.updatingPack = true;
            this.packedSimcardNumber = response.pack;
            this.simService.getPack(response.pack).subscribe((pack) => {
              this.simcards = pack;
              this.simcards.forEach((sim) => {
                this.updateState(sim);
              });
              this.loader = false;
              this.simNumber = '';
              this.simNumberInput.nativeElement.focus();
            });
          } else {
            if (!this.simExists(response)) {
              response.packOrder = this.simcards.length + 1;
              this.simcards.push(response);
              this.updateState(response);
              this.simNumber = '';
              this.simNumberInput.nativeElement.focus();
            } else {
              this.notifierService.notify('error', 'La Simcard ya se encuentra agregada');
            }
            this.loader = false;
          }
        } else {
          this.notifierService.notify('error', 'Simcard no encontrada');
          this.loader = false;
        }
      }, error => {
        this.loader = false;
        this.results = false;

        if (error.status === 404) {
          this.notifierService.notify('error', 'Simcard no encontrada');
        } else {
          this.notifierService.notify('error', 'Lo sentimos, ocurrio un error');
        }
      });
    }
  }

  updateState(simcard) {
    const today = new Date().toISOString();
    if (simcard.activationDate === undefined && simcard.dueDate > today) {
      this.states[simcard.number] = 'Disponible';
    } else if (simcard.activationDate !== undefined && simcard.dueDate > today) {
      this.states[simcard.number] = 'Activada';
    } else if (simcard.dueDate < today) {
      this.states[simcard.number] = 'Vencida';
    }
  }

  simExists(sim: Sim) {
    for (const simcard of this.simcards) {
      if (simcard.number === sim.number && simcard.icc === sim.icc) {
        return true;
      }
    }
    return false;
  }

  removeSim(simcard) {
    for (let i = this.simcards.length - 1; i >= 0; i--) {
      if (this.simcards[i].number === simcard.number) {
        this.simcards.splice(i, 1);
        this.toDeleteFromPack.push(simcard);
        break;
      }
    }
  }

  packSims() {
    this.loader = true;
    this.simService.packSims(this.simcards).subscribe((response) => {
      this.packedSimcardNumber = response;
      this.resumeSimcards = this.simcards;
      this.simcards = [];
      this.resumeStates = this.states;
      this.states = [];
      if (response[0] !== undefined) {
        this.subSelected = response[0].subDistributorName;
        this.refreshSubdistributor();
        this.wholesalerSelected = response[0].employeeIdentification;
      } else {
        this.refreshSubdistributor();
      }
      this.notifierService.notify('success', 'Simcards Empaquetadas Satisfactoriamente');
      this.loader = false;
    }, (error => {
      this.notifierService.notify('error', 'Lo sentimos, ocurrio un error');
      console.error(error);
    }));
  }

  updatePack() {
    this.simService.updatePack(this.packedSimcardNumber, this.simcards).subscribe((response) => {
      this.resumeSimcards = this.simcards;
      this.simcards = [];
      this.resumeStates = this.states;
      this.states = [];
      this.notifierService.notify('success', 'Paquete Actualizado Satisfactoriamente');
      this.subSelected = response[0].subDistributorName;
      this.refreshSubdistributor();
      this.wholesalerSelected = response[0].employeeIdentification;
      if (this.toDeleteFromPack.length > 0) {
        this.simService.cleanSimPack(this.toDeleteFromPack).subscribe(() => {
          this.myStepper.next();
        });
      } else {
        this.myStepper.next();
      }
    }, (error => {
      this.notifierService.notify('error', 'Lo sentimos, ocurrio un error');
      console.error(error);
    }));
    this.myStepper.next();
  }

  assignPack() {
    this.loader = true;
    this.simService.assignPack(this.packedSimcardNumber, this.wholesalerSelected).subscribe((response) => {
      this.loader = false;
      this.myStepper.reset();
      this.notifierService.notify('success', 'Paquete Asignado Satisfactoriamente');
    }, (error => {
      this.notifierService.notify('error', 'Lo sentimos, ocurrio un error');
      console.error(error);
    }));
  }

  unassignPack() {
    this.loader = true;
    this.simService.unassignPack(this.simcards[0].pack).subscribe((response) => {
      this.loader = false;
      this.myStepper.reset();
      this.notifierService.notify('success', 'Paquete Desasignado Satisfactoriamente');
      this.clean();
    }, (error => {
      this.notifierService.notify('error', 'Lo sentimos, ocurrio un error');
      console.error(error);
    }));
  }

  clean() {
    this.simcards = [];
    this.states = [];
    this.resumeSimcards = [];
    this.resumeStates = [];
    this.myStepper.reset();
    this.updatingPack = false;
    this.packedSub = '';
    this.toDeleteFromPack = [];
    this.newPack = true;
  }
}
