import {Component, OnInit} from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {ReportsService} from '../../../../core/services/reports.service';
import {DistributorService} from '../../../../core/services/distributor.service';
import {DatePipe} from '@angular/common';
import {UserService} from '../../../../core/services/user.service';

@Component({
  selector: 'bell-free-report',
  templateUrl: './freeReport.component.html',
  styleUrls: ['./freeReport.component.sass']
})
export class FreeReportComponent implements OnInit {

  distributors: any[];
  distributorName;
  loader: boolean;
  initialDate: Date;
  finalDate: Date;
  employeeRole;

  constructor(private formBuilder: FormBuilder, private userService: UserService, private reportService: ReportsService,
              private distributorService: DistributorService, private datePipe: DatePipe) {
  }

  ngOnInit() {
    this.employeeRole = this.userService.userData.role;
    this.distributorName = this.userService.userData.subdistributorName;
    this.loader = false;
    this.distributorService.getdistributorList()
      .subscribe(response => {
        this.distributors = response;
        this.distributors.push({
          'name': 'TODOS'
        });
      });
  }

  downloadReport() {
    this.loader = true;
    const initialDateFormatted = this.datePipe.transform(this.initialDate, 'yyyy-MM-dd');
    const finalDateFormatted = this.datePipe.transform(this.finalDate, 'yyyy-MM-dd');
    this.reportService.downloadSimcardsAssignationByDistributorReport({
      type: 2,
      subDistributorName: this.distributorName,
      initialDate: initialDateFormatted,
      finalDate: finalDateFormatted
    })
      .subscribe((response) => {
        const url = window.URL.createObjectURL(response);
        const a = document.createElement('a');
        document.body.appendChild(a);
        a.setAttribute('style', 'display: none');
        a.href = url;
        a.download = 'reporteLibres.csv';
        a.click();
        window.URL.revokeObjectURL(url);
        a.remove();
        this.loader = false;
      });
  }

}
