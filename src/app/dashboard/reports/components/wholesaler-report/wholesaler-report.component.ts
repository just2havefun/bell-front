import {Component, OnInit} from '@angular/core';
import {Wholesaler} from '../../../../shared/models/wholesaler.model';
import {Router} from '@angular/router';
import {UserService} from '../../../../core/services/user.service';
import {ReportsService} from '../../../../core/services/reports.service';
import {SubDistributorService} from '../../../../core/services/subdistributor.service';

@Component({
  selector: 'bell-wholesaler-report',
  templateUrl: './wholesaler-report.component.html',
  styleUrls: ['./wholesaler-report.component.sass']
})
export class WholesalerReportComponent implements OnInit {

  userType: string;
  employeeRole: string;
  subSelected: string;
  wholesalerSelected: number;
  subList: Array<string>;
  employeeList: Array<Wholesaler>;
  loader = false;

  constructor(private router: Router, private userService: UserService, private reportsService: ReportsService,
              private subDisService: SubDistributorService) {
  }

  ngOnInit() {
    this.subList = this.userService.subList;
    this.subSelected = this.userService.userData.subdistributorName;
    this.userType = this.userService.userData.type;
    this.employeeRole = this.userService.userData.role;

    if (this.employeeRole === 'ADMIN' || this.userType === 'SUBDISTRIBUTOR') {
      this.refreshSubDistributor();
    } else {
      this.wholesalerSelected = this.userService.userData.employee.identification;
      this.downloadReport();
    }
  }

  refreshSubDistributor() {
    this.loader = true;
    this.subDisService.getWholesalersByLeader(this.subSelected)
      .subscribe((response) => {
        this.employeeList = response.wholesalers;
        this.loader = false;
      });
  }

  downloadReport() {
    this.loader = true;
    this.reportsService.downloadSimcardsByEmployeeReport(this.wholesalerSelected)
      .subscribe((response) => {
        const url = window.URL.createObjectURL(response);
        const a = document.createElement('a');
        document.body.appendChild(a);
        a.setAttribute('style', 'display: none');
        a.href = url;
        a.download = 'reporteMayorista.csv';
        a.click();
        window.URL.revokeObjectURL(url);
        a.remove();
        this.loader = false;
      });
  }

}
