import {Component, OnInit} from '@angular/core';
import {FreeReportComponent} from './components/freeReport/freeReport.component';
import {MatDialog} from '@angular/material';
import {WholesalerReportComponent} from './components/wholesaler-report/wholesaler-report.component';
import {PrepaidReportComponent} from './components/prepaid-report/prepaid-report.component';
import {UserService} from '../../core/services/user.service';
import {WholesalerCommissionComponent} from './components/wholesaler-commission/wholesaler-commission.component';

@Component({
  selector: 'bell-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.sass']
})
export class ReportsComponent implements OnInit {

  COMPONENTS = [
    {
      name: 'Asignación Prepagos',
      component: PrepaidReportComponent,
      visible: this.userService.userData.type === 'SUBDISTRIBUTOR' || this.userService.userData.role === 'ADMIN'
    },
    {
      name: 'Asignación Libres',
      component: FreeReportComponent,
      visible: this.userService.userData.type === 'SUBDISTRIBUTOR' || this.userService.userData.role === 'ADMIN'
    },
    {
      name: 'Asignación Mayorista',
      component: WholesalerReportComponent,
      visible: true
    },
    {
      name: 'Comisión Mayorista',
      component: WholesalerCommissionComponent,
      visible: true
    }
  ];

  constructor(public dialog: MatDialog, private userService: UserService) {
  }

  ngOnInit() {
  }

  openDialog(component) {
    this.dialog.open(component, {
      width: '400px',
      panelClass: 'paper-background'
    });
  }

}
