import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UserService} from '../../../core/services/user.service';
import {MatDialogRef} from '@angular/material';

@Component({
  selector: 'bell-restore-password',
  templateUrl: './restore-password.component.html',
  styleUrls: ['./restore-password.component.sass']
})
export class RestorePasswordComponent implements OnInit {

  loginForm: FormGroup;
  loader: boolean;

  constructor(private formBuilder: FormBuilder, private userService: UserService,
              private dialogRef: MatDialogRef<RestorePasswordComponent>) {
    this.loader = false;
    this.loginForm = this.formBuilder.group({
      userName: [null, [Validators.required]],
      password: [null, [Validators.required]]
    });
  }

  ngOnInit() {
  }

  reset() {
    const email = this.loginForm.get('userName').value;
    this.userService.restorePassword(email)
      .subscribe(response => {
        alert('Exitoso');
        this.dialogRef.close();
      });
  }
}
