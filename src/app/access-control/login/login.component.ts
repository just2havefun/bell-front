import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

import {ApplicationUser} from '../../shared/models/application-user.model';
import {UserService} from '../../core/services/user.service';
import {SubDistributorService} from '../../core/services/subdistributor.service';
import {MatDialog} from '@angular/material';
import {RestorePasswordComponent} from './restore-password/restore-password.component';
import {NotifierService} from 'angular-notifier';

@Component({
  selector: 'bell-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  loader: boolean;

  constructor(private router: Router, private formBuilder: FormBuilder, private userService: UserService,
              private subServie: SubDistributorService, private dialog: MatDialog, public notifierService: NotifierService) {
  }

  ngOnInit() {
    this.loader = false;
    this.loginForm = this.formBuilder.group({
      userName: [null, [Validators.required]],
      password: [null, [Validators.required]]
    });
  }

  logIn() {
    this.loader = true;
    const user: ApplicationUser = {};
    user.email = this.loginForm.get('userName').value;
    user.password = this.loginForm.get('password').value;

    this.userService.logIn(user).subscribe(response => {
      sessionStorage.setItem('ut', response.jwtToken);
      this.userService.userData.email = user.email;

      this.userService.getUser(user.email).subscribe(userData => {
        this.userService.userData = userData;
        this.subServie.getSubList().subscribe(subList => {
          const subs = [];
          subList.forEach(element => {
            subs.push(element.name);
          });
          this.router.navigate(['/dashboard/search']);
          this.userService.setSubList(subs);
        }, () => {
          this.notifierService.notify('error', 'Usuario o contraseña no validos');
        });
      }, () => {
        this.notifierService.notify('error', 'Usuario o contraseña no validos');
      });
    }, () => {
      this.loader = false;
      this.notifierService.notify('error', 'Usuario o contraseña no validos');
    });
  }

  restorePassword() {
    this.dialog.open(RestorePasswordComponent, {
      width: '400px',
      panelClass: 'paper-background'
    });
  }
}
