import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RoutingModule} from './routing.module';
import {ReactiveFormsModule} from '@angular/forms';
import {SharedModule} from '../shared/shared.module';
import {LoginComponent} from './login/login.component';
import {RestorePasswordComponent} from './login/restore-password/restore-password.component';

@NgModule({
  imports: [
    CommonModule,
    RoutingModule,
    ReactiveFormsModule,
    SharedModule
  ],
  entryComponents: [
    RestorePasswordComponent
  ],
  declarations: [
    LoginComponent,
    RestorePasswordComponent
  ]
})
export class AccessControlModule {
}
