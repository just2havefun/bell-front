import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { SessionGuard } from './core/guards/session-guard.service';

const ROUTES: Routes = [
    {
        path: '',
        loadChildren: 'app/access-control/access-control.module#AccessControlModule'
    },
    {
      path: 'dashboard',
      canActivateChild: [SessionGuard],
      loadChildren: 'app/dashboard/dashboard.module#DashboardModule'
    },
    {
        path: 'error',
        loadChildren: 'app/shared/shared.module#SharedModule'
    },
    {
      path: '**',
      redirectTo: '/'
    }
];

@NgModule({
  imports: [
    RouterModule.forRoot(ROUTES),
  ],
  exports: [
    RouterModule
  ]
})
export class RoutingModule {}
