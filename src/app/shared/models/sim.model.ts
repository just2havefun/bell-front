export class Sim {
  activationDate?: string;
  deliveryDate?: string;
  dueDate?: string;
  employeeIdentification?: string;
  employeeName?: string;
  icc?: string;
  number?: string;
  pack?: number;
  type?: number;
  typeName?: string;
  subDistributorName?: string;
  packOrder?: number;
  plan?: string;
  value?: string;
}
