export class Production {
  employeeIdentification: number;
  employeeName: string;
  prepaidProduction: number;
  freeProduction: number;
}
