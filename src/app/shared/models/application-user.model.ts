import {Wholesaler} from './wholesaler.model';

export class ApplicationUser {
  email?: string;
  employee?: Wholesaler;
  subdistributorName?: string;
  password?: string;
  type?: string;
  role?: string;
}
