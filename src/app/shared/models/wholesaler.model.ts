export class Wholesaler {
  identification?: number;
  name?: string;
  locatable?: boolean;
  phone?: string;
  address?: string;
  commission?: string;
  leaderName?: string;
  role?: string;
}
