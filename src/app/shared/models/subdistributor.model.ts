import { Wholesaler } from './wholesaler.model';

export class Subdistributor {
  name?: string;
  email?: string;
  commission?: string;
  wholesalers?: Wholesaler;
}
