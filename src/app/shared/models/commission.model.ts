export class Commission {
  employeeIdentification: number;
  employeeName: string;
  prepaidCommission: number;
  prepaidIca: number;
  prepaidRete: number;
  freeCommission: number;
  freeIca: number;
  freeRete: number;
}
