import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Router} from '@angular/router';

import {UserService} from '../../../core/services/user.service';

@Component({
  selector: 'bell-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.sass']
})
export class SideMenuComponent implements OnInit {

  @Input()
  display: boolean;
  @Output()
  close = new EventEmitter();

  subsLoaded = false;
  isAdmin = false;
  isSub = false;

  constructor(private router: Router, private userService: UserService) {
    this.subsLoaded = this.userService.subsLoaded;
    this.isAdmin = this.userService.userData.role === 'ADMIN';
    this.isSub = this.userService.userData.type === 'SUBDISTRIBUTOR';
  }

  ngOnInit() {
  }

  closeMenu(): void {
    this.close.emit();
  }

  redirect(route: string): void {
    this.router.navigate([route]);
    this.closeMenu();
  }

  logOut(): void {
    this.userService.logOut();
    this.router.navigate(['/']);
  }
}
