import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Router} from '@angular/router';
import {UserService} from '../../../core/services/user.service';

@Component({
  selector: 'bell-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.sass']
})
export class HeaderComponent implements OnInit {

  subsLoaded = false;
  employeeName: string;
  isAdmin = false;
  isSub = false;

  @Output()
  menu = new EventEmitter();

  constructor(private router: Router, private userService: UserService) {
    this.subsLoaded = this.userService.subsLoaded;
    this.isAdmin = this.userService.userData.role === 'ADMIN';
    this.isSub = this.userService.userData.type === 'SUBDISTRIBUTOR';
  }

  ngOnInit() {
    this.userService.subsLoadedEmitter.subscribe((value) => {
      this.subsLoaded = value;
    });
    if (this.userService.userData.type === 'SUBDISTRIBUTOR' || this.userService.userData.role === 'ADMIN') {
      this.employeeName = this.userService.userData.subdistributorName;
    } else {
      this.employeeName = this.userService.userData.employee.name;
    }
  }

  openMenu(): void {
    this.menu.emit();
  }

  redirect(): void {
    this.router.navigate(['/']);
  }
}
