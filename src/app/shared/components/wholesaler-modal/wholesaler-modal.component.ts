import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

import {Wholesaler} from '../../models/wholesaler.model';
import {EmployeeService} from '../../../core/services/employee.service';

@Component({
  selector: 'bell-wholesaler-modal',
  templateUrl: './wholesaler-modal.component.html',
  styleUrls: ['./wholesaler-modal.component.sass']
})
export class WholesalerModalComponent implements OnInit, OnChanges {

  @Input() display: boolean;
  @Input() data: Wholesaler;
  @Input() leader: string;
  @Output() close: EventEmitter<any> = new EventEmitter();

  hide: boolean;
  infoForm: FormGroup;
  newWholesaler: Wholesaler = {};

  constructor(private formBuilder: FormBuilder, private employeeService: EmployeeService) {
  }

  ngOnInit() {
    this.infoForm = this.formBuilder.group({
      identification: [null, [Validators.required]],
      name: [null, [Validators.required]],
      locatable: [null, [Validators.required]],
      phone: [null, [Validators.required]],
      address: [null, [Validators.required]],
      commission: [null, [Validators.required]]
    });
  }

  ngOnChanges() {
    if (this.data !== null && this.data !== undefined) {
      this.infoForm.get('identification').setValue(this.data.identification);
      this.infoForm.get('name').setValue(this.data.name);
      this.infoForm.get('locatable').setValue(this.data.locatable);
      this.infoForm.get('phone').setValue(this.data.phone);
      this.infoForm.get('address').setValue(this.data.address);
      this.infoForm.get('commission').setValue(this.data.commission + '%');
    }
  }

  closeModal() {
    this.close.emit();
  }

  updateInfo() {
    this.newWholesaler.identification = this.infoForm.get('identification').value;
    this.newWholesaler.name = this.infoForm.get('name').value;
    this.newWholesaler.locatable = this.infoForm.get('locatable').value;
    this.newWholesaler.phone = this.infoForm.get('phone').value;
    this.newWholesaler.address = this.infoForm.get('address').value;
    this.newWholesaler.commission = this.infoForm.get('commission').value;
    this.newWholesaler.leaderName = this.leader;
    this.newWholesaler.role = 'WHOLESALER';
    this.employeeService.updateWholesaler(this.newWholesaler).subscribe(response => {

    }, error => {

    });
  }
}
