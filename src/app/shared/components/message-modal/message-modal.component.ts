import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'bell-message-modal',
  templateUrl: './message-modal.component.html',
  styleUrls: ['./message-modal.component.sass']
})
export class MessageModalComponent implements OnInit {

  message: string;
  type: string;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any) {
  }

  ngOnInit() {
    this.type = this.data.type;
    this.message = this.data.message;
  }

}
