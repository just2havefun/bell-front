export function formatNumber(number) {
  const formatter = new Intl.NumberFormat('es-CO', {
    minimumFractionDigits: 0
  });
  return formatter.format(number);
}

export function formatMoney(number) {
  const formatter = new Intl.NumberFormat('es-CO', {
    style: 'currency',
    currency: 'COP',
    minimumFractionDigits: 0
  });
  return formatter.format(number);
}
