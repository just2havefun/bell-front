import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatRadioModule} from '@angular/material/radio';
import {MatCardModule} from '@angular/material/card';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatSelectModule} from '@angular/material/select';
import {MatTableModule} from '@angular/material/table';
import {MatDialogModule} from '@angular/material/dialog';

import {HeaderComponent} from './components/header/header.component';
import {WholesalerModalComponent} from './components/wholesaler-modal/wholesaler-modal.component';
import {SideMenuComponent} from './components/side-menu/side-menu.component';
import {MessageModalComponent} from './components/message-modal/message-modal.component';
import {
  MatCheckboxModule,
  MatListModule,
  MatNativeDateModule,
  MatProgressBarModule,
  MatSliderModule,
  MatStepperModule
} from '@angular/material';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {RouterModule} from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    MatInputModule,
    MatButtonModule,
    MatIconModule,
    MatExpansionModule,
    MatRadioModule,
    MatCardModule,
    MatProgressSpinnerModule,
    MatSelectModule,
    MatTableModule,
    MatDialogModule,
    MatCheckboxModule,
    MatSliderModule,
    ReactiveFormsModule,
    MatTableModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatStepperModule,
    MatProgressBarModule,
    MatListModule,
    FormsModule,
    RouterModule
  ],
  declarations: [HeaderComponent, WholesalerModalComponent, SideMenuComponent, MessageModalComponent],
  exports: [
    MatInputModule,
    MatButtonModule,
    MatIconModule,
    MatExpansionModule,
    MatRadioModule,
    MatCardModule,
    MatProgressSpinnerModule,
    MatSelectModule,
    MatTableModule,
    MatDialogModule,
    MatCheckboxModule,
    MatSliderModule,
    HeaderComponent,
    WholesalerModalComponent,
    SideMenuComponent,
    MatTableModule,
    MatStepperModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatProgressBarModule,
    MatListModule,
    MessageModalComponent,
    RouterModule
  ],
  entryComponents: [
    MessageModalComponent
  ]
})
export class SharedModule {
}
