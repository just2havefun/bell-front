import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {UserService} from '../services/user.service';
import 'rxjs/add/operator/catch';
import {NotifierService} from 'angular-notifier';

@Injectable()
export class SessionGuard implements CanActivate {

  constructor(private userService: UserService, private router: Router, public notifierService: NotifierService) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    const url = route.routeConfig.path;
    const status = this.userService.isLogIn();
    const user = this.userService.userData;

    if (status && user !== null && user !== undefined) {
      if (url === 'commissions') {
        const subdistributorName = this.userService.userData.subdistributorName;

        if ((this.userService.userData.employee !== null && this.userService.userData.employee !== undefined) ||
          (subdistributorName !== null && subdistributorName !== undefined)) {
        } else {
          this.notifierService.notify('error', 'Lo sentimos, ocurrio un error. Intentelo nuevamente');
        }
      }
      return true;
    } else {
      this.router.navigate(['/']);
    }
  }

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.canActivate(route, state);
  }
}
