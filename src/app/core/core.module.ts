import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {UserService} from './services/user.service';
import {SimService} from './services/sim.service';
import {SubDistributorService} from './services/subdistributor.service';
import {ReportsService} from './services/reports.service';

import {SessionGuard} from './guards/session-guard.service';
import {DistributorService} from './services/distributor.service';
import {EmployeeService} from './services/employee.service';
import {UploadService} from './services/upload.service';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [],
  providers: [
    UserService,
    SimService,
    EmployeeService,
    DistributorService,
    SubDistributorService,
    ReportsService,
    UploadService,
    SessionGuard
  ]
})
export class CoreModule { }
