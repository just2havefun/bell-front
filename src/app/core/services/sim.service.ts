import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {map} from 'rxjs/operators';
import {Sim} from '../../shared/models/sim.model';

@Injectable()
export class SimService {

  constructor(private httpClient: HttpClient) {
  }

  getSimInfo(identification: string): Observable<Sim> {
    return this.httpClient.get<Sim>('/v1/simcard/' + identification)
      .pipe(map((response: Sim) => response));
  }

  assignSim(icc: string, employeeIdentification: string): Observable<any> {
    return this.httpClient.put('/v1/simcard/assign/' + icc, {'employeeIdentification': employeeIdentification})
      .pipe(map((response: Response) => response));
  }

  unassignSim(icc: string): Observable<any> {
    return this.httpClient.put('/v1/simcard/unassign/' + icc, {})
      .pipe(map((response: Response) => response));
  }

  packSims(data) {
    return this.httpClient.post('/v1/simcard/pack', data)
      .pipe(map((response: Response) => response));
  }

  assignPack(packNumber, employeeIdentification) {
    return this.httpClient.put('/v1/simcard/assign/pack/' + packNumber + '/' + employeeIdentification, {})
      .pipe(map((response: Response) => response));
  }

  unassignPack(packNumber) {
    return this.httpClient.put('/v1/simcard/unassign/pack/' + packNumber, {})
      .pipe(map((response: Response) => response));
  }

  getPack(packNumber) {
    return this.httpClient.get<Sim[]>('/v1/simcard/pack/' + packNumber)
      .pipe(map((response: Sim[]) => response));
  }

  updatePack(packNumber, simcards) {
    return this.httpClient.put('/v1/simcard/pack/' + packNumber, simcards)
      .pipe(map((response: Response) => response));
  }

  cleanSimPack(simNumbers) {
    return this.httpClient.post('/v1/simcard/pack/clean', simNumbers)
      .pipe(map((response: Response) => response));
  }
}
