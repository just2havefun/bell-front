import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {map} from 'rxjs/operators';
import {Wholesaler} from '../../shared/models/wholesaler.model';

@Injectable()
export class EmployeeService {

  basePath = '/v1/employee';

  constructor(private httpClient: HttpClient) {
  }

  listWholesalers(): Observable<any> {
    return this.httpClient.get(this.basePath + '/wholesalers')
      .pipe(map((response: Response) => response));
  }

  listWholesalersByLeader(leaderName): Observable<any> {
    return this.httpClient.get(this.basePath + '/wholesalers/' + leaderName)
      .pipe(map((response: Response) => response));
  }

  createWholesaler(wholesaler: Wholesaler): Observable<any> {
    return this.httpClient.post(this.basePath, wholesaler)
      .pipe(map((response: Response) => response));
  }

  updateWholesaler(wholesaler: Wholesaler): Observable<any> {
    return this.httpClient.put('/v1/employee/' + wholesaler.identification, wholesaler)
      .pipe(map((response: Response) => response));
  }

  deleteWholesaler(wholesaler: Wholesaler): Observable<any> {
    return this.httpClient.delete('/v1/employee/' + wholesaler.identification)
      .pipe(map((response: Response) => response));
  }
}
