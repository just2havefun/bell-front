import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {map} from 'rxjs/operators';
import {Commission} from '../../shared/models/commission.model';
import {Production} from '../../shared/models/production.model';

@Injectable()
export class ReportsService {

  constructor(private httpClient: HttpClient) {
  }

  // Get periods available by identification
  getAvailablePeriods(identification: number): Observable<any> {
    return this.httpClient.get('/v1/commission/period/' + identification)
      .pipe(map((response: Response) => response));
  }

  // Get periods available by subname
  getAvailablePeriodsByName(name: string): Observable<any> {
    return this.httpClient.get('/v1/commission/sub/period/' + name)
      .pipe(map((response: Response) => response));
  }

  // Ganancias totales BARRAS
  getCommissions(identification: number): Observable<any> {
    return this.httpClient.get('/v1/commission/' + identification)
      .pipe(map((response: Response) => response));
  }

  // Ganancias
  getCommissionsByPeriod(identification: number, period: string): Observable<any> {
    return this.httpClient.get('/v1/commission/' + identification + '/' + period)
      .pipe(map((response: Response) => response));
  }

  getCommissionByPeriodSubs(name: string, period: string): Observable<Commission[]> {
    return this.httpClient.get<Commission[]>('/v1/commission/sub/' + name + '/' + period)
      .pipe(map((response: Commission[]) => response));
  }

  // Proyeccion
  getProjectionByPeriod(identification: number, period: string): Observable<any> {
    return this.httpClient.get('/v1/commission/projection/' + identification + '/' + period)
      .pipe(map((response: Response) => response));
  }

  // Producción

  getProductionAvailablePeriodsByName(name: string): Observable<any> {
    return this.httpClient.get('/v1/production/period/' + name)
      .pipe(map((response: Response) => response));
  }

  getProductionByPeriod(identification: number, period: string): Observable<any> {
    return this.httpClient.get('/v1/production/' + identification + '/' + period)
      .pipe(map((response: Response) => response));
  }

  getProductionByPeriodSubs(name: string, period: string): Observable<Production[]> {
    return this.httpClient.get<Production[]>('/v1/production/sub/' + name + '/' + period)
      .pipe(map((response: Production[]) => response));
  }

  // Report Asignación Simcards Libres
  downloadSimcardsAssignationByDistributorReport(data): Observable<Blob> {
    return this.httpClient.post('/v1/report/simcard/assignation', data, {responseType: 'blob'});
  }

  // Reporte Asignación mayorista
  downloadSimcardsByEmployeeReport(employeeIdentification: number): Observable<Blob> {
    return this.httpClient.get('/v1/report/simcard/employee/' + employeeIdentification, {responseType: 'blob'});
  }

  // Reporte Comisiones mayorista
  downloadWholesalerCommissionReport(employeeIdentification: number, period: string): Observable<Blob> {
    return this.httpClient.get('/v1/commission/download/' + employeeIdentification + '/' + period, {responseType: 'blob'});
  }

  // Recharges Report
  getSubRechargeReportByPeriod(subdistributor: string, period: string): Observable<any> {
    return this.httpClient.get('/v1/recharge/' + subdistributor + '/' + period);
  }

}
