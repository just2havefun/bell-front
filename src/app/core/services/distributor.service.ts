import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {map} from 'rxjs/operators';

@Injectable()
export class DistributorService {

  constructor(private httpClient: HttpClient) {
  }

  getdistributorList(): Observable<any> {
    return this.httpClient.get('/v1/distributor')
      .pipe(map((response: Response) => response));
  }

  getSubDis(dis: string): Observable<any> {
    return this.httpClient.get('/v1/distributor/email/' + dis)
      .pipe(map((response: Response) => response));
  }
}
