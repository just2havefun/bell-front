import { Injectable, Injector } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import { environment } from '../../../environments/environment';

@Injectable()
export class HttpInterceptorService implements HttpInterceptor {

  constructor() { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const token = sessionStorage.getItem('ut') || '';
    const url = this.updateUrl(req.url);
    const authReq = req.clone({
      headers: req.headers.set('Authorization', token),
      url: url
    });

    return next.handle(authReq)
      .catch((error, caught) => {
        return Observable.throw(error);
      }) as any;
  }

  private updateUrl(url: string) {
    const urlRegexp = new RegExp(/^http/);
    if (urlRegexp.test(url)) {
      return url;
    }
    return environment.baseUrl + url;
  }
}
