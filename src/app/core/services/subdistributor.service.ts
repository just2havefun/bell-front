import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {map} from 'rxjs/operators';

@Injectable()
export class SubDistributorService {

  constructor(private httpClient: HttpClient) {
  }

  getSubList(): Observable<any> {
    return this.httpClient.get('/v1/subdistributor')
      .pipe(map((response: Response) => response));
  }

  getWholesalersByLeader(subdis: string): Observable<any> {
    return this.httpClient.get('/v1/subdistributor/' + subdis)
      .pipe(map((response: Response) => response));
  }

  getWholesalersByLeaderMail(mail: string): Observable<any> {
    return this.httpClient.get('/v1/subdistributor/email/' + mail)
      .pipe(map((response: Response) => response));
  }

  updateSubDistributor(subDistributor): Observable<any> {
    return this.httpClient.put('/v1/subdistributor', subDistributor)
      .pipe(map((response: Response) => response));
  }
}
