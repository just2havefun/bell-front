import {Injectable} from '@angular/core';
import {HttpClient, HttpEvent, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';


@Injectable()
export class UploadService {

  constructor(private httpClient: HttpClient) {
  }

  // Report Asignación Simcards Libres
  uploadUpdateNumbersByIccFile(file, email): Observable<HttpEvent<{}>> {
    const formData: FormData = new FormData();
    formData.append('file', file[0]);
    formData.append('receiver', email);

    const req = new HttpRequest('POST', '/v1/upload/numbers-icc', formData);

    return this.httpClient.request(req);

  }

}
