import {EventEmitter, Injectable, Output} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {map} from 'rxjs/operators';

import {ApplicationUser} from '../../shared/models/application-user.model';

@Injectable()
export class UserService {

  userData: ApplicationUser = {};
  subList: Array<string>;
  subsLoaded = false;

  @Output() subsLoadedEmitter: EventEmitter<boolean> = new EventEmitter();

  constructor(private httpClient: HttpClient) {
  }

  createUser(applicationUser: ApplicationUser): Observable<any> {
    return this.httpClient.post('/v1/user', applicationUser)
      .pipe(map((response: Response) => response));
  }

  changePassword(pswd: string): Observable<any> {
    const updatedUser = this.userData;
    updatedUser.password = pswd;
    return this.httpClient.put('/v1/user/' + updatedUser.email, updatedUser)
      .pipe(map((response: Response) => response));
  }

  restorePassword(email): Observable<any> {
    return this.httpClient.get('/v1/user/restore/' + email)
      .pipe(map((response: Response) => response));
  }

  getUser(mail: string): Observable<any> {
    return this.httpClient.get('/v1/user/' + mail)
      .pipe(map((response: Response) => response));
  }

  logIn(user: ApplicationUser): Observable<any> {
    return this.httpClient.post('/login', user)
      .pipe(map((response: Response) => response));
  }

  isLogIn(): boolean {
    let status;
    const token = sessionStorage.getItem('ut');

    token ? status = true : status = false;
    return status;
  }

  logOut(): void {
    sessionStorage.removeItem('ut');
  }

  setSubList(subList) {
    this.subList = subList;
    this.subsLoaded = true;
    this.subsLoadedEmitter.emit(true);
  }


}
